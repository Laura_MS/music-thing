//a single note
const note = function(abcNote) {
    this.note = 0;
    this.octave = 0;
    this.length = 0;
    note.prototype.playNote = function(note,octave,length,timeOffset) {
        var synth = new Tone.Synth().toMaster();
        synth.triggerAttackRelease(`${note}${octave}`, `${length}`, timeOffset);
    }
}

//check if the tune has [ or ], representing simultaneous notes. i don't want to deal with that
const hasBrackets = function(tuneJSON) {
    	return false;
}

const displayInfo = function(name) {
    document.getElementById("title").innerText = name;
}

const displayNotes = function(notesObjArray) {
    var tuneSection = document.getElementById("tune-section"), noteDiv,headDiv,headText,tailDiv,color,downShift,minShift=1050;
    for(var i=0; i<notesObjArray.length; i++) {
        downShift = 0;
        switch(notesObjArray[i].octave) { //4 is highest
            case 1: color = "red";
                    downShift += 280*3;
                break;
            case 2: color = "green";
                    downShift += 280*2;
                break;
            case 3: color = "blue";
                    downShift += 280;
                break;
            case 4: color = "purple";
                break;
        }

        switch(notesObjArray[i].note) { //c is lowest
            case "c":
            case "C": downShift += 210;
                break;
            case "d":
            case "D": downShift += 175;
                break;
            case "e":
            case "E": downShift += 140;
                break;
            case "f":
            case "F": downShift += 105;
                break;
            case "g":
            case "G": downShift += 70;
                break;
            case "a":
            case "A": downShift += 35;
                break;
            case "b":
            case "B": downShift += 0;
                break;
        }

        if(downShift < minShift) { minShift = downShift; }

        noteDiv = document.createElement("span");
        noteDiv.setAttribute("class",`note ${color}`);
        noteDiv.style.position = "relative";
        noteDiv.style.top = downShift;

        headDiv = document.createElement("div");
        headDiv.setAttribute("class","head");
        headText = document.createTextNode(notesObjArray[i].note);
        headDiv.appendChild(headText);

        tailDiv = document.createElement("div");
        tailDiv.setAttribute("class","tail");
        tailDiv.style.width = 400 * notesObjArray[i].length;

        noteDiv.appendChild(headDiv);
        noteDiv.appendChild(tailDiv);

        tuneSection.appendChild(noteDiv);
        tuneSection.style.position="relative";
        tuneSection.style.top = -minShift;
    }
}
 
const parseTune = function(tuneJSON) {
    var regex = /[A-Ga-g][',]?\d?/g;
    var noteArray = tuneJSON.match(regex);
    var noteObjArray = [];
    var basicNoteLength = 0.25;

    console.log(noteArray);

    for(var i=0; i<noteArray.length; i++) {
        let current = new note();
        current.note = noteArray[i][0];

        if(/[A-G]{1}[,]{1}/.test(noteArray[i])) { //A-G,
            current.octave = 1;
        }
        else if(/[a-g]{1}[']{1}/.test(noteArray[i])) { //a-g'
            current.octave = 4;
        }
        else if(/[A-G]{1}/.test(noteArray[i])) { //A-G
            current.octave = 2;
        }
        else if(/[a-g]{1}/.test(noteArray[i])) { //a-g
            current.octave = 3;
        }

        if(/[/]{1}[\d]{1,2}/.test(noteArray[i])) { //slash followed by number - divide bnl by number
            current.length = basicNoteLength/parseInt(noteArray[i].substr(1,noteArray[i].length-1));
        }
        else if(/[/]{1}/.test(noteArray[i])) { //just a slash - half bnl
            current.length = basicNoteLength/2;
        }
        else if(/[\d]{1,2}/.test(noteArray[i])) { //just a number
            current.length = basicNoteLength*parseInt(noteArray[i].match(/[\d]/)[0]);
        }
        else { //no modifier
            current.length = basicNoteLength;
        }

        noteObjArray.push(current);
    }

    console.log(noteObjArray);
    displayNotes(noteObjArray);
    playAllNotes(noteObjArray);
}

const getTune = function() {
    var r = Math.floor(Math.random()*17794);
    var url = `https://thesession.org/tunes/${r}?format=json`;
    var goodTune = false;


    fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(responseJson) {
        console.log(responseJson);
        parseTune(responseJson.settings[0].abc);
        displayInfo(titleFix(responseJson.name));
    });
}

const titleFix = function(title) { //is there a better way to do this?
    title = title.replace("&#039;", "'");
    return title;
}

const calcScore = function() {
    //notes hit / total notes * 100
}

const setBadges = function() {
    
}

const getBadges = function() {

}

const cursorHandler = function() {
    var cursor = document.getElementsByClassName("cursor")[0];
    var box = document.getElementsByClassName("box")[0];
    var score = 0;
    document.addEventListener("mousemove", function(e) {
        if(e.clientY >= box.getBoundingClientRect().top && e.clientY <= box.getBoundingClientRect().bottom-27) {
            //https://stackoverflow.com/questions/6593447/snapping-to-grid-in-javascript
            cursor.style.top = 35 * Math.floor( (e.clientY-11) /35);
        }
    });

    //add a hitcheck listener
}

const playAllNotes = function(noteObjArray) { //using a prototype function instead of a standard function makes it lag a lot
    var timeOffset = 0;

    //https://developer.mozilla.org/en-US/docs/Web/API/Window/getComputedStyle
    //var tsWidth = window.getComputedStyle(document.getElementById("tune-section")).getPropertyValue("width");
    document.getElementById("playButton").addEventListener("click", function() {  //requires a user gesture
        cursorHandler();
        for(var i=0; i<noteObjArray.length; i++) { //queueing notes to be played
            noteObjArray[i].playNote(noteObjArray[i].note, noteObjArray[i].octave, noteObjArray[i].length, timeOffset);
            timeOffset += noteObjArray[i].length;
        }

        //console.log(window.innerWidth, document.getElementById("tune-section").getBoundingClientRect().right);
        var scrollHandler = setInterval(function() {
            document.getElementById("tune-section").scrollBy(20,0);
            //console.log(tsWidth, document.getElementById("tune-section").scrollLeft);
            //https://developer.mozilla.org/en-US/docs/Web/API/Window/scrollX
            if(document.getElementById("tune-section").getBoundingClientRect().right <= 0) {
                console.log("stop");
                clearInterval(scrollHandler);
            }
            //if(document.getElementById("tune-section").getBoundingClientRect().right <= window.innerWidth) { clearInterval(scrollHandler); }
        }, 250);
    });
}

var tuneJSON = getTune();